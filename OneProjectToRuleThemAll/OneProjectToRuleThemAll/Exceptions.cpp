#include "Exceptions.h"

unsigned int getAgeGod(int input_majorGods) noexcept
{
	return 1 + input_majorGods / 2;
}

void testExceptions()
{
	std::cout << "You have advanced to the Mythic Age through the Vindication of Horus.\n";

	unsigned int input_age(0);
	int input_majorGods(0);
	std::cout << "What Age.\n";
	std::cin >> input_age;
	std::cout << "Which major God.\n";
	std::cin >> input_majorGods;

	try 
	{
		if (input_majorGods == 0) throw -1;
		if (input_majorGods < 0) throw std::out_of_range("Negative");
		double code(input_age / input_majorGods);
		std::cout << code << "\n" << getAgeGod(input_majorGods) << "\n" ;
	}
	catch (int& codeError)
	{
		std::cout << "Error : " << codeError << std::endl;
	}
	catch (std::out_of_range& codeError)
	{
		std::cout << "Error : " << codeError.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Error\n";
	}
	


}