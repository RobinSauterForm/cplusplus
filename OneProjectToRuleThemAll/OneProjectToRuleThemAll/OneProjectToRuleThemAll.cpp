#include <iostream>

#include "bases.h"
#include "functionCPP.h"
#include "cPtr.h"
#include "testClass.h"
#include "operatorC.h"
#include "Heritage.h"
#include "Exceptions.h"
#include "TemplateT.h"
#include "stl.h"

int main(int /* argc */, char* /* argv */[])
{
    std::cout << "They are rage, brutal, without mercy. But you. You will be worse. Rip and tear, until it is done.\n";
    std::cout << "I'm willing to take full responsibility for the horrible events of the last 24 hours," 
        << "but you must understand - our interest in their world was purely for the betterment of mankind."
        << "Everything has clearly gotten out of hand, yes, but it was worth the risk. I assure you.\n";

    /*
    cType();
    cCondition();
    cLoop();
    cTab();
    */

    paramFunc();

    musicDoom("BFG Division", "Mick Gordon", 8.26);
    musicDoom("Mastermind", "Mick Gordon");

    varFunc();

    ptrTab();
    tabFunc();

    reference();

    baseObj();

    testHorde();

    DarkSoulsBoss::visibilityOp();

    testHeritage();

    //testExceptions();

    testTemplate();

    collection();


    return EXIT_SUCCESS;
}

