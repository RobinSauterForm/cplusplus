#include "TemplateT.h"


//const int CTE = int(420);
template<typename T> const T PI = T(3.1415798563785219658825);

template<typename T> 
void display(T x, T y)
{
	std::cout << x << " , " << y << std::endl;
}

template<typename T>
T getX()
{
	std::cout << "X ? : ";
	T x;
	std::cin >> x;
	return x;
}


template<class T>
T somme(T x, T y, T z)
{
	return x + y + z;
}

template<class T1, class T2>
decltype(auto)
mult(T1 x, T2 y, T2 z)
/*->decltype(x*y*z) */ 
{
	return x * y * z;
}


template<class T1, class T2>
decltype(auto)
mult2(T1 x, T2 y)
{
	return x * y;
}

/*
template<typename>
std::string mult2(std::string x, unsigned int y)
{
	return "";
}
*/

template<typename T> 
struct Conteneur
{
	Conteneur() : v() {};
	Conteneur(T x) : v(x) {};

	template<typename T2>
	bool equalApp(T2 n)
	{
		return (n > v - 1) && (n < v + 1);
	}

	T v;
};

template<>
struct Conteneur<std::string>
{
	Conteneur() : v("default") {};
	Conteneur(const std::string& x) : v(x) {};

	std::string v;
};

namespace ml
{

template<int V>
unsigned int mult3(unsigned int x)
{
	return x * V;
}

}


void testTemplate()
{
	std::cout << "You died in my kitchen, Alana, when you chose to be brave. Every moment since has been borrowed. Your wife, your child, they belong to me. You made a bargain for Will's life, and then I spun you gold.\n";
	std::cout << "Remarkable boy. I do admire your courage. I think I�ll eat your heart.\n";

	float piF(PI<float>);
	float piD(PI<double>);
	float piI(PI<int>);

	std::cout << "Well done.Here come the test results : You are a horrible person.[subtitles read : I'm serious;] That's what it says: a horrible person. We weren't even testing for that.\n";

	std::cout << "Float Pi : " << piF << std::endl;
	std::cout << "Double Pi : " << piD << std::endl;
	std::cout << "Int Pi : " << piI << std::endl;

	display<double>(4.0639, 53.98);
	display("Level 3 Research Associate Scientist", "HEV Suit");

	std::vector<int> v1;

	std::cout << "Warning: Neurotoxin pressure has reached dangerously unlethal levels.\n";

	std::cout << getX<double>() << std::endl;
	std::cout << somme(1,2,3) << std::endl;
	std::cout << somme(std::string("z"), std::string("y"), std::string("x")) << std::endl;
	std::cout << mult(2.95, 5, 6) << std::endl;

	std::cout << "Oh, in case you get covered in that Repulsion Gel, here's some advice the lab boys gave me: [sound of rustling pages] Do not get covered in the Repulsion Gel. We haven't entirely nailed down what element it is yet, but I'll tell you this: It's a lively one, and it does not like the human skeleton." << std::endl;

	Conteneur<float> a, b(1.9654f);
	a.v = 1.6189f;

	std::cout << a.equalApp(1) << std::endl;

	std::cout << "I'm in space. Spaaace! I'm in space. I'm in space." << std::endl;

	//std::cout << mult2(std::string("11"),(unsigned int)3) << std::endl;

	Conteneur<std::string> c("The end");
	std::cout << c.v << std::endl;
	const int cc(5);
	std::cout << ml::mult3<cc>(5) << std::endl;
	
}
