#include "testClass.h"

#include <iostream>
#include <random>


Demon::Demon()
: Demon
(
	"",
	1.0,
	1.0
)
{

}


Demon::Demon(const std::string& name)
: Demon
(
	name,
	1.0,
	1.0
)
{

}

Demon::Demon
(
	const std::string& argname,
	double arghp,
	double argdamage
)
: name(argname), hp(arghp), damage(argdamage)
{

}

Demon::Demon(const Demon& d)
{
	if (this != &d)
	{
		name = d.name;
		hp = d.hp;
		damage = d.hp;
	}
}

Demon::~Demon()
{
	std::cout << "The Slayer killed " + name << std::endl;
}

void Demon::display()
{
	std::cout << "Demon struct : " << name << " , HP : " << hp << " , Damage : " << damage << std::endl;
};

void Demon::rageBoost(double coef)
{
	hp *= coef;
	damage *= coef;
}
void Demon::reduc(double coef)
{
	rageBoost(1.0 / coef);
}


void baseObj()
{
	std::cout << "Butt stallion says Hello.\n";
	std::cout << "Size doesn't matter. Except when it does. Which is always.\n";

	Demon pinky;
	pinky.name = "Pinky";
	pinky.hp = 100.0;
	pinky.damage = 10.0;

	std::cout << "Demon struct : " << pinky.name << " , HP : " << pinky.hp << " , Damage : " << pinky.damage << std::endl;

	Demon* imp = new Demon;
	imp->name = "Imp";
	imp->hp = 10.0;
	imp->damage = 2.5;

	std::cout << "Demon struct : " << imp->name << " , HP : " << imp->hp << " , Damage : " << imp->damage << std::endl;

	Demon whiplash("Whiplash", 200.0, 15.0);
	whiplash.display();
	whiplash.rageBoost(2.0);
	whiplash.display();
	whiplash.reduc(2.0);
	whiplash.display();

	Demon arachnotron{ "Arachnotron", 500.0, 50.0 };

	delete imp;
}




Horde::Horde()
: Horde(100)
{

}

Horde::Horde(size_t numberOfDemon)
: m_numberOfDemon(numberOfDemon), m_vectDemon(new Demon[numberOfDemon])
{

}

Horde::Horde(size_t numberOfDemon, Demon* vectDemon)
: m_numberOfDemon(numberOfDemon), m_vectDemon(vectDemon)
{

}

Horde::Horde(const Horde& h)
: m_numberOfDemon(h.m_numberOfDemon), m_vectDemon(new Demon[m_numberOfDemon])
{
	for (size_t i(0); i < m_numberOfDemon; i++)
	{
		m_vectDemon[i].name = h.m_vectDemon[i].name;
		m_vectDemon[i].hp = h.m_vectDemon[i].hp;
		m_vectDemon[i].damage = h.m_vectDemon[i].damage;
	}
}

Horde::Horde(Horde&& h)
: m_numberOfDemon(h.m_numberOfDemon),
  m_vectDemon(h.m_vectDemon)
{
	h.m_vectDemon = nullptr;
}

Horde::~Horde()
{
	if (nullptr != m_vectDemon)
	{
		delete[] m_vectDemon;
		m_vectDemon = nullptr;
	}
}

Horde& Horde::operator=(const Horde& h)
{
	if (this != &h)
	{
		m_numberOfDemon = h.m_numberOfDemon;

		if (nullptr != m_vectDemon)
		{
			delete[] m_vectDemon;
			m_vectDemon = nullptr;
		}
		m_vectDemon = new Demon[m_numberOfDemon];

		for (size_t i(0); i < m_numberOfDemon; i++)
		{
			m_vectDemon[i].name = h.m_vectDemon[i].name;
			m_vectDemon[i].hp = h.m_vectDemon[i].hp;
			m_vectDemon[i].damage = h.m_vectDemon[i].damage;
		}
	}
	return *this;
}

void Horde::display()
{
	for (size_t i(0); i < m_numberOfDemon; i++)
	{
		m_vectDemon[i].display();
	}
}

Horde getOneHorde()
{
	Horde h{1};
	h.m_vectDemon[0].name = "Cacodemon";
	return h;
}

void testHorde()
{
	const size_t NUMBER_OF_DEMONS(5);

	Horde horde(NUMBER_OF_DEMONS);

	std::mt19937 randGenerator;
	std::uniform_real_distribution<double> hp(10.0, 1000.0);
	std::uniform_real_distribution<double> damage(10.0, 75.0);

	for (size_t i(0); i < horde.m_numberOfDemon; i++)
	{
		horde.m_vectDemon[i].name = "Default";
		horde.m_vectDemon[i].hp = hp(randGenerator);
		horde.m_vectDemon[i].damage = damage(randGenerator);
		horde.m_vectDemon[i].display();
	}


	std::cout << "The Arachnotron's rear-mounted turret is capable of targeting the Slayer from extreme ranges and can deal extensive damage in a short period of time" << std::endl;


	Horde h2(horde);

	h2.m_vectDemon[0].name = "Slayer";
	h2.m_vectDemon[0].hp = 10000.0;
	h2.m_vectDemon[0].damage = 9000.0;

	horde = h2;

	horde.display();

	std::cout << "From medium to close distances, it may also use its grenade launchers, firing bouncing balls of energy that explode after a brief time. When right next to the player, the Arachnotron will swipe at them with one of its legs." << std::endl;


	Horde h3 = getOneHorde();
	h3.display();
}