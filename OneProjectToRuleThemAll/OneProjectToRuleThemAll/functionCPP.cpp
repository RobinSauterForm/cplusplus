#include "functionCPP.h"

#include <iostream>
#include <functional>


void paramFunc()
{
	std::cout << "If I'm to choose between one evil and another, then I prefer not to choose at all.\n";

	std::cout << "Number of Bullet needed in one Map :" << getNumberOfBulletToKill(100, 10) 
		<< std::endl << "Number of Bullet needed in all game :" << getNumberOfBulletToKill(100, 10, 50) << std::endl;

	std::cout << "Bioshock may be infinite, Doom may be eternal, but the glory of Rome is forever.\n";
}

unsigned int getNumberOfBulletToKill
(
	unsigned int numberOfDemons,
	unsigned int bulletToKillOne
)
{
	return numberOfDemons * bulletToKillOne;
}

unsigned int getNumberOfBulletToKill
(
	unsigned int numberOfDemons,
	unsigned int bulletToKillOne,
	unsigned int inAllMap
)
{
	return numberOfDemons * bulletToKillOne * inAllMap;
}

void musicDoom
(
	const std::string& name,
	const std::string& aut,
	double time /* = 10.0 */
)
{
	std::cout << "Music : " + name + " , aut : " + aut + " , time : " + std::to_string(time) + "m" << std::endl;
}


void varFunc()
{
	std::function<void(const std::string&, const std::string&, double)> func1(musicDoom);
	auto func2(func1);
	func2("Battle Of The Heroes / Duel Of The Fates", "John Williams", 7.02);

	std::function<void()> f1(std::bind(musicDoom, "Dark Souls III Soundtrack OST-Soul of Cinder", "FromSoftware", 5.52));
	f1();

	std::function<void(double)> f2(std::bind(musicDoom, "Age of Mythology Soundtrack", "Stephen Rippy & Kevin McMullan", std::placeholders::_1));
	f2(46.31);

	std::function<void(const std::string&,double)> f3(std::bind(musicDoom, "The Elder Scrolls III: Morrowind Soundtrack (Full)", std::placeholders::_1, std::placeholders::_2));
	f3("Jeremy Soule", 46.33);

	std::function<void(double, const std::string&)> f4(std::bind(musicDoom, "Elder Scrolls IV: Oblivion Soundtrack", std::placeholders::_2, std::placeholders::_1));
	f4(59.30, "Jeremy Soule");
}