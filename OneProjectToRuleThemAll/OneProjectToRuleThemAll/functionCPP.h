#ifndef functionCPP
#define functionCPP

#include <string>

void paramFunc();

unsigned int getNumberOfBulletToKill
(
	unsigned int numberOfDemons,
	unsigned int bulletToKillOne
);

unsigned int getNumberOfBulletToKill
(
	unsigned int numberOfDemons,
	unsigned int bulletToKillOne,
	unsigned int inAllMap
);

void musicDoom
(
	const std::string& name,
	const std::string& aut,
	double time = 10.0
);

void varFunc();

#endif // !functionCPP