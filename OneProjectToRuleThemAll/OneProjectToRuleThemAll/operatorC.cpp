#include "operatorC.h"

#include <iostream>


DarkSoulsBoss::DarkSoulsBoss()
: DarkSoulsBoss("Default","Not far", 1)
{

}

DarkSoulsBoss::DarkSoulsBoss
(
	const std::string& name,
	const std::string& location,
	unsigned int difficulty
)
: m_name(name), m_location(location), m_difficulty(difficulty)
{

}

DarkSoulsBoss::~DarkSoulsBoss()
{

}

DarkSoulsBoss DarkSoulsBoss::operator+(const DarkSoulsBoss& d)const
{
	DarkSoulsBoss fusion(m_name + " - " + d.m_name, m_location, m_difficulty * d.m_difficulty);
	return fusion;
}

bool DarkSoulsBoss::operator==(const DarkSoulsBoss& d)const
{
	if (m_name.compare(d.m_name) == IDENTICAL_STRINGS)
	{
		return true;
	}
	return false;
}

bool DarkSoulsBoss::operator!=(const DarkSoulsBoss& d)const
{
	return !(*this == d);
}

void DarkSoulsBoss::operator()()const
{
	display();
}


void DarkSoulsBoss::display()const
{
	std::cout << m_name << " , location "
			  << m_location << " , difficulty : "
			  << m_difficulty << std::endl;
}




void DarkSoulsBoss::visibilityOp()
{
	DarkSoulsBoss aldrich("Aldrich, Devourer of Gods", "Anor Londo", 10);
	DarkSoulsBoss pontiffSulyvahn("Pontiff Sulyvahn", "Irithyll of the Boreal Valley", 8);

	aldrich.display();
	DarkSoulsBoss fusion(aldrich + pontiffSulyvahn);

	if (aldrich == pontiffSulyvahn)
	{
		std::cout << "Comp valid\n";
	}
	else
	{
		std::cout << "Comp invalid\n";
	}
	fusion();
}