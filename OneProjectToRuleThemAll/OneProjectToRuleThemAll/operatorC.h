#ifndef operatorC_H
#define operatorC_H

#include <string>

#define IDENTICAL_STRINGS 0

class DarkSoulsBoss
{
public:
	DarkSoulsBoss();
	DarkSoulsBoss
	(
		const std::string& name,
		const std::string& location,
		unsigned int difficulty
	);

	~DarkSoulsBoss();

public:

	DarkSoulsBoss operator+(const DarkSoulsBoss& d)const;
	bool operator==(const DarkSoulsBoss& d)const;
	bool operator!=(const DarkSoulsBoss& d)const;
	void operator()()const;

public:

	void display()const;

public:

	static void visibilityOp();

private:
	std::string m_name;
	std::string m_location;
	unsigned int m_difficulty;
};

#endif // !operatorC_H
