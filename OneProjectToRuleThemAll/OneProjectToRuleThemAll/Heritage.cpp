#include "Heritage.h"

/* --------------------------------- Display ---------------------------------- */

class Display
{
public:

	virtual void display() = 0;

};

/* --------------------------------- AOM_Unit ---------------------------------- */

class AOM_Unit : public Display /* Age Of Mythology */
{
public:
	AOM_Unit();
	/*AOM_Unit() = default;*/
	/*AOM_Unit() = delete;*/
	AOM_Unit
	(
		std::string name,
		factionType faction,
		std::string word
	);

public:

	virtual void display() override;
	void displayWithMagisticGraphic();

protected:

	void emptyName() { m_name = ""; };

protected:

	std::string m_name;
	factionType m_faction;
	std::string m_word;
};

AOM_Unit::AOM_Unit()
:
	AOM_Unit
	(
		"Villager",
		factionType::Greeks,
		"Prostagma"
	)
{

}

AOM_Unit::AOM_Unit
(
	std::string name,
	factionType faction,
	std::string word
)
: m_name(name), m_faction(faction), m_word(word)
{

}

void AOM_Unit::display()
{
	std::cout << m_name << " , " << m_word << std::endl;
}
void AOM_Unit::displayWithMagisticGraphic()
{
	std::cout << "## ";
	display();
	std::cout << " ##" << std::endl;
}

/* --------------------------------- AOM_Hero ---------------------------------- */

class AOM_Hero : virtual public AOM_Unit
{
public:
	AOM_Hero
	(
		std::string name,
		factionType faction,
		std::string word,
		double aoeBonus
	);

public:

	using AOM_Unit::emptyName;

public:

	virtual void display() /* final */ override;

protected:
	double m_aoeBonus;
};

AOM_Hero::AOM_Hero
(
	std::string name,
	factionType faction,
	std::string word,
	double aoeBonus
)
 :AOM_Unit(name, faction, word), m_aoeBonus(aoeBonus)
{

}

void AOM_Hero::display()
{
	AOM_Unit::display();
	std::cout << "m_aoeBonus : " << m_aoeBonus << std::endl;
}


/* --------------------------------- AOM_TITAN ---------------------------------- */

class AOM_TITAN : virtual public AOM_Unit
{
public:
	AOM_TITAN(): AOM_TITAN(1){};
	AOM_TITAN
	(
		unsigned int maxNumberOfSpecialAttacks
	)
		: m_maxNumberOfSpecialAttacks(maxNumberOfSpecialAttacks)
	{};

public:

	virtual void display()override
	{
		std::cout << "maxNumberOfSpecialAttacks : "
			<< m_maxNumberOfSpecialAttacks << std::endl;
	};

protected:
	unsigned int m_maxNumberOfSpecialAttacks;
};

/* --------------------------------- AOM_TITAN ---------------------------------- */

class AOM_GOD : public AOM_Hero, public AOM_TITAN
{
public:
	AOM_GOD()
		:AOM_GOD
		(
			"Osiris", 
			factionType::Egyptians, 
			"I have come upon the Earth and with my two feet taken possession!",
			100.0,
			5,
			true
		) 
	{};

	AOM_GOD
	(
		std::string name,
		factionType faction,
		std::string word,
		double aoeBonus,
		unsigned int maxNumberOfSpecialAttacks,
		bool canFly
	)
	  : AOM_Unit(name, faction, word),
		AOM_Hero(name, faction, word, aoeBonus),
		AOM_TITAN(maxNumberOfSpecialAttacks),
		m_canFly(canFly)
	{};

	virtual void display()override
	{
		AOM_Hero::display();
		AOM_TITAN::display();
		std::cout << "canFly : " << m_canFly << std::endl;
	};

protected:
	bool m_canFly;
};

/* --------------------------------- test ---------------------------------- */

void testHeritage()
{
	std::cout << "It's time to kick ass and chew bubble gum... and I'm all outta gum.\n";

	AOM_Unit cyclops("Cyclops", factionType::Greeks, "kalos");
	cyclops.display();

	AOM_Unit villager("Villager", factionType::Greeks, "Maista");
	villager.displayWithMagisticGraphic();

	AOM_Hero amanra("Amanra", factionType::Egyptians, "Hesvole", 4.0);
	amanra.displayWithMagisticGraphic();
	amanra.emptyName();

	AOM_Hero gargarensis("Gargarensis", factionType::Greeks, "This is interesting. I would have thought you smarter than to ignore my warning. Fortunately, there are other routes to Tartarus, so this one can be closed. ", 10.0);
	gargarensis.displayWithMagisticGraphic();

	AOM_GOD athena
	(
		"Athena",
		factionType::Greeks,
		"You have done well. Now I must prepare you for the rest of your journey.",
		100.0,
		5,
		false
	);
	athena.AOM_Hero::displayWithMagisticGraphic();
	
	std::cout << sizeof(AOM_Unit) << " , "
		<< sizeof(AOM_Hero) << " , "
		<< sizeof(AOM_TITAN) << " , "
		<< sizeof(AOM_GOD) << std::endl;
}
