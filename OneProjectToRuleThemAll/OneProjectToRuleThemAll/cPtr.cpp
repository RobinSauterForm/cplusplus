#include "cPtr.h"

#include <iostream>

void ptrTab()
{
	std::cout << "If I'm to choose between one evil and another, then I prefer not to choose at all.\n";

	unsigned int doot(42);
	unsigned int* ptrDoot(&doot);
	unsigned int** ptrptrDoot(&ptrDoot);

	std::cout << "doot : " << doot << " , ptr doot : " << ptrDoot << ", ptr ptr doot : " << ptrptrDoot << std::endl;
	doot++;
	std::cout << "doot : " << doot << " , ptr doot : " << ptrDoot << ", ptr ptr doot : " << ptrptrDoot << std::endl;
	std::cout << "doot : " << **ptrptrDoot << " , ptr doot : " << *ptrptrDoot << ", ptr ptr doot : " << ptrptrDoot << std::endl;

	const unsigned int SIZE(5);
	float pro[SIZE] = { 0.0f,1.5f,2.88f,3.99f,405.63f };
	float* p(nullptr);
	p = &pro[0];
	if(nullptr != p)
		*p = 10.099f;
	std::cout << "4e val : " << pro[3] << " , 4e val : " << *(p + 3) << std::endl;

	float sum(0);
	for (unsigned int i(0); i < SIZE; i++)
	{
		std::cout << i << "e val : " << *(p + i) << " , sum : " << (sum += *(p + i)) << std::endl;
	}
		
}

unsigned int minProv(unsigned int provinceToConvert[], unsigned int size)
{
	unsigned int min = 4500;
	for (unsigned int i(0); i < size; i++)
	{
		if (min > provinceToConvert[i])
		{
			min = provinceToConvert[i];
		}
	}
	return min;
}

unsigned int* algoTab(unsigned int provinceToConvert[], const unsigned int size)
{
	unsigned int* tab(new unsigned int[size]);
	for (unsigned int i(0); i < size; i++)
	{
		tab[i] = i * provinceToConvert[i];
	}
	return tab;
}

void tabFunc()
{
	std::cout << "There is no destiny... It does not exist. The only thing that everyone is destined for is death.\n";
	std::cout << "An Empire to convert to Shinto /* EU4 */.\n";

	unsigned int provinceToConvert[] = { 236 , 183 , 184 , 196 , 201 , 257 };
	std::cout << "min province to convert tot shinto : " << minProv(provinceToConvert, 6) << std::endl;

	unsigned int* p(algoTab(provinceToConvert, 6));

	if (nullptr != p)
	{
		delete[] p;
		p = nullptr;
	}
	
}


void displayUHD(unsigned int& refblueRayUHD)
{
	refblueRayUHD *= 10;
	std::cout << " Ref " << refblueRayUHD << std::endl;
}

unsigned int& arrondi(unsigned int b)
{
	b = 25 * unsigned int(b / 25);
	return b;
}

unsigned int getVal() { return 9; }

std::string form(const std::string& s)
{
	std::string res = " - " + s + " - ";
	return res;
}


std::string form(std::string&& s)
{
	s = " - " + s + " - ";
	return s;
}

void reference()
{
	std::cout << "They are not demons, not devils... Worse than that.They are people.\n";
	unsigned int blueRayUHD(500);
	unsigned int& refblueRayUHD(blueRayUHD);
	refblueRayUHD++;
	std::cout << refblueRayUHD << std::endl;

	unsigned int&& h = getVal();

	std::string title("Mandate of Heaven - Confucian");
	std::string m(form(title));
	std::cout << title + " , " + m + "\n";
	std::cout << form("Catholic - Protestant - Reformed - Orthodox - Anglican - Hussite - Coptic") << std::endl;
}
