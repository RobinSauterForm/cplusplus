#ifndef testClass_H
#define testClass_H

#include <string>
#include <iostream>

void baseObj();

struct Demon
{
	Demon();
	~Demon();
	Demon(const std::string& name);
	Demon(const std::string& argname, double arghp, double argdamage);
	Demon(const Demon& d);

	void display();
	void rageBoost(double coef);
	void reduc(double coef);


	std::string name = "";
	double hp = 0.0;
	double damage = 0.0;
};

struct Horde
{
	Horde();
	Horde(size_t numberOfDemon);
	Horde(size_t numberOfDemon, Demon* vectDemon);

	Horde(const Horde& h);
	Horde(Horde&& h);

	~Horde();

	Horde& operator=(const Horde& F);
	
	void display();

	size_t m_numberOfDemon = 0;
	Demon* m_vectDemon = nullptr;
};

void testHorde();


#endif // !testClass_H



