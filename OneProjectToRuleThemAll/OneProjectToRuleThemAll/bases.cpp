

#include "bases.h"

#include <iostream>
#include <string>

void cType()
{
    int fear(-42);
    unsigned int solution(42'666);
    int binA(0x9);
    std::cout << sizeof(fear) << " bytes\n";

    char single('C');
    short little(255);
    long longg(42069);
    long long veryLong(666);
    std::cout << sizeof(single) << " bytes , " << sizeof(little) << " bytes , " << sizeof(longg) << " bytes , " << sizeof(veryLong) << " bytes\n";

    float cacodemonKilled(4211.8f);
    float revenantKilled(3.12e9);

    double totalKilledA(double(cacodemonKilled) + (double)revenantKilled);
    bool pepsi(false);
    bool deadnt = (!pepsi) && (totalKilledA > 100.0);

    std::string imp("imp");


}

void cCondition()
{
    unsigned int toKill(82300);
    toKill = toKill + 1;
    toKill += 1;
    ++toKill;
    toKill++;

    unsigned int map(870 / 11);
    unsigned int toKillMap((toKill + map - 1) / map);

    unsigned int remain(toKillMap * map - toKill);



    if (toKill < 42)
    {
        std::cout << "iS yOu Ok, SlAyEr????";
    }
    else
    {
        std::cout << "The only thing they fear is you : to Kill/map : " << std::to_string(toKillMap) << " , remaining : " << std::to_string(remain) << std::endl;
    }

    unsigned int needBFG_ammo(toKillMap / 260);
    switch (needBFG_ammo)
    {
    case 0:
    case 1:
    case 2:
        std::cout << "The Slayer will enter the facility\n";
        break;
    case 3:
        std::cout << "The Slayer is in the facility\n";
        break;
    default:
        std::cout << "There is no Slayer... will return when enough BFG ammo\n";
    }

    if (50'000 > toKill)
    {
        std::cout << "I�m Too Young to Die\n";
    }
    else if (150'000 >= toKill && 50'000 < toKill)
    {
        std::cout << "Hurt Me Plenty\n";
    }
    else if (300'000 >= toKill && 150'000 < toKill)
    {
        std::cout << "Ultra-Violence\n";
    }
    else if (300'000 < toKill)
    {
        std::cout << "Nightmare\n";
    }
    else
    {
        exit(EXIT_FAILURE);
    }
}

void cLoop()
{
    unsigned int toKillMap(1042);
    unsigned int d(0);
    while (d < toKillMap)
    {
        std::cout << "Load 100 demons\n";
        d += 100;
    }

    d = 0;
    do
    {
        std::cout << "Load 200 demons\n";
        d += 200;
    } while (d < toKillMap);

    for (unsigned int i(0); i < toKillMap; i += 300)
    {
        std::cout << "Load 300 demons\n";
    }

}

void cTab()
{
    const unsigned int NUMBER_OF_DEMONS(6);
    float demonsHP[NUMBER_OF_DEMONS] = { 3000.0f, 53.0f, 999.0f, 9985.9f, 10.0f, -995.0f };

    float maxHP(-10000.0f), minHP(99999.0f);
    float indexMaxHP(0), indexMinHP(0);
    for (unsigned int i(0); i < NUMBER_OF_DEMONS; i++)
    {
        if (demonsHP[i] < minHP)
        {
            minHP = demonsHP[i];
            indexMinHP = i;
        }
        else if (demonsHP[i] > maxHP)
        {
            maxHP = demonsHP[i];
            indexMaxHP = i;
        }
    }

    std::cout << "minHP = " << indexMinHP << " , maxHP = " << indexMaxHP;
    //demonsHP[10] = 3000.10; code erreur

    float demonsHPmax[NUMBER_OF_DEMONS][2];

    demonsHPmax[0][0] = 3000.10f;
    demonsHPmax[0][1] = 2900.10f;

}