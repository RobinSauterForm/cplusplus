#ifndef Heritage_H
#define Heritage_H

#include <iostream>
#include <string>


enum class factionType
{
	Greeks,
	Egyptians,
	Norse,
	Atlanteans
};

void testHeritage();

#endif // !Heritage_H
