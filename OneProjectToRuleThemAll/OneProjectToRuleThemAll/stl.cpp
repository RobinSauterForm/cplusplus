#include "stl.h"

#include <iostream>
#include <vector>
#include <array>
#include <list>
#include <deque>
#include <map>
#include <algorithm>
#include <memory>

bool assertSize(const std::string& s)
{
	return s.size() > 20;
}

void collection()
{
	std::cout << "In the first age, in the first battle, when the shadows first lengthened, one stood.\n";

	/* initializer_list<T> */
	std::vector<std::string> title{ "Sekiro - Shadows Die Twice", "Demon Souls" };
	title.push_back("Dark Souls");
	title.push_back("Elden Ring"); /* Soon ... */

	title[0] += "";

	std::cout << "titles size : " << title.size() << std::endl;

	for (const auto l : title)
	{
		std::cout << l << std::endl;
	}

	std::cout << "Burned by the embers of Armageddon, his soul blistered by the fires of Hell and tainted beyond ascension, he chose the path of perpetual torment.\n";

	std::vector<std::string> titleCopy;
	for (auto i(0); i < title.size(); i++)
	{
		if ((i % 2) == 0)
		{
			titleCopy.push_back(title[i]);
		}
	}

	std::vector<std::string> titleCopy2;
	for (auto it(title.cbegin()); it != title.cend(); it += 2)
	{
		titleCopy2.push_back(*it);
	}

	for (auto it(titleCopy.cbegin()); it != titleCopy.cend(); it++)
	{
		std::cout << *it << std::endl;
	}

	for (const auto l : titleCopy2)
	{
		std::cout << l << std::endl;
	}

	std::cout << "In his ravenous hatred, he found no peace, and with boiling blood he scoured the Umbral Plains seeking vengeance against the dark lords who had wronged him.\n";
	
	std::array<std::string, 3> arrFitGirlRepack 
	{
		"BioShock",
		"BioShock 2",
		"BioShock Infinite - The Complete Edition"
	};

	std::list<std::string> AssassinsCreed
	{ 
		"1",
		"2",
		"BrotherHood",
		"Revelation",
		"3",
		"Black Flag", 
		"Rogue",
		"Unity", 
		"Syndicate",
		"Origin",
		"Odyssey",
		"Valhalla"
	};

	std::deque<double> r{ 2300.01, 1444.01, 1936.01, 769.01 };
	
	std::cout << "He wore the crown of the Night Sentinels, and those that tasted the bite of his sword named him... the Doom Slayer.\n";

	std::map<double, std::string> mapAssassinsCreed
	{
		{1.0, "1"},
		{2.0, "2"},
		{2.1, "BrotherHood"},
		{2.2, "Revelation"},
		{3.0, "3"},
		{4.0, "Black Flag"},
		{4.5, "Rogue"},
		{5.0, "Unity"},
		{6.0, "Syndicate"},
		{7.0, "Origin"},
		{8.0, "Odyssey"},
		{9.0, "Valhalla"}
	};

	std::cout << "Tempered by the fires of Hell, his iron will remain steadfast through the passage that preys upon the weak.\n";

	std::vector<std::string> titleL(title.size());
	//std::copy_if(title.begin(), title.end(), titleL.begin(), assertSize);
	std::copy_if(title.begin(), title.end(), titleL.begin(), [](const std::string& s) {return s.size() > 20; });

	for (const auto l : titleL)
	{
		std::cout << l << std::endl;
	}

	std::cout << 
	std::any_of
	(
		title.begin(),
		title.end(),
		[](const std::string& s) {return s.find('y', 0) != std::string::npos; })
	<< std::endl;


	std::vector<std::unique_ptr<std::string>> vecptrString;
	vecptrString.push_back(std::make_unique<std::string>("UAC REPORT FILE H8UM66S"));



}